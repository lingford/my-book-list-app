# My Book List App

* Use form fields to add new books to your list
* Click red x to remove books from your list
* App uses JavaScript ES6 Syntax
* Uses browser localstorage for saving booklist
* Uses Bootstrap css for styling of app
